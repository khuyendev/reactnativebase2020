const AppRoute = {
    AUTH_LOADING: "AUTH_LOADING",
    LOGIN_ROUTE: "LOGIN_ROUTE",
    HOME_ROUTE: "HOME_ROUTE",
}
const LoginRoute = {
    LOGIN: "LOGIN",
    REGISTER: "REGISTER",
}
const HomeRoute = {
    HOME: "HOME",
}
export {
    AppRoute,
    LoginRoute,
    HomeRoute,
}
