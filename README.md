This project was development by **Amela Technology**

Below you'll find information about performing common tasks.

## Table of Contents

## Integration to new project
1. Create new React Native project. Please read [the official React Native document](https://facebook.github.io/react-native/docs/getting-started) for more information
2. Copy this file/folder listed below from templet project to your new project:
   - File package.json
        - Copy all element `script`, `devDependencies`, `jest`
        - `dependencies`: exclude react and react-native
        - Run `npm assets-link`  for link assets to native project
        - Run `npm outdate` for update new version dependencies
   - File babel.config.js, jest.config.js, metro.config.js, react-native-config.js
   - Folder src (keep all file contain) 
## Available Scripts

If Yarn was installed when the project was initialized, then dependencies will have been installed via Yarn, and you should probably use it to run these commands as well. Unlike dependency installation, command running syntax is identical for Yarn and NPM at the time of this writing.

### `npm start`

Runs your app in development mode.

```
npm start -- --reset-cache
# or
yarn start -- --reset-cache
```

#### `npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup). We also recommend installing Genymotion as your Android emulator. 
