const BASE_URL = __DEV__ ? "http://localhost:8000/api" : "http://api.example.co/api"
export const MAX_TIMEOUT_REQUEST = 40000

export const URL = {
    URL_EXAMPLE: `${BASE_URL}/retailer`,
}
