const Light = {
    colors: {
        primary: "#607d8b",
        secondary: "#607d8b",
        textPrimary: "#607d8b",
        textSecondary: "#607d8b",
    },
    fonts: {
        defaultFont: "Montserrat-Regular",
        boldFont: "Montserrat-SemiBold",
        thinFont: "Montserrat-Light",
    },
}

const Dark = {
    colors: {
        primary: "#607d8b",
        secondary: "#607d8b",
        textPrimary: "#607d8b",
        textSecondary: "#607d8b",
    },
    fonts: {
        defaultFont: "Montserrat-Regular",
        boldFont: "Montserrat-SemiBold",
        thinFont: "Montserrat-Light",
    },
}
export const Themes = Light
